#Primjer 6.1
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

from sklearn.preprocessing import MinMaxScaler
# min-max skaliranje
sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

# inicijalizacija i ucenje KNN modela
KNN_model = KNeighborsClassifier(n_neighbors = 5)
KNN_model.fit(X_train_n, y_train)

# inicijalizacija i ucenje SVM modela
SVM_model = svm.SVC(kernel='rbf', gamma = 1, C=0.1)
SVM_model.fit(X_train_n, y_train)

# predikcija na skupu podataka za testiranje
y_test_p_KNN = KNN_model.predict(X_test)
y_test_p_SVM = SVM_model.predict(X_test)
print(y_test_p_KNN)
print(y_test_p_SVM)

#Primjer 6.2
from sklearn.model_selection import cross_val_score
model = svm.SVC(kernel='linear', C=1, random_state=42)
scores = cross_val_score(model, X_train, y_train, cv=5)
print(scores)


#Primjer 6.3
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV

# Define the pipeline with preprocessing and classifier
pipe = Pipeline([
    ('scaler', StandardScaler()),  # Step 1: Standardization
    ('model', svm.SVC())               # Step 2: SVM classifier
])

param_grid = {'model__C': [10, 100, 100],
'model__gamma': [10, 1, 0.1, 0.01]}
svm_gscv = GridSearchCV(pipe, param_grid, cv=5, scoring='accuracy', n_jobs=-1)
svm_gscv.fit(X_train, y_train)
print(svm_gscv.best_params_)
print(svm_gscv.best_score_)
print(svm_gscv.cv_results_)