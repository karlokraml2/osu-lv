"""Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji
sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovarajucu poruku."""

def Average(lst): 
    return sum(lst) / len(lst)

numbers = []
count = 0

while(True): 
    x = input("Unesi broj ili Done: ")
    if x == "Done":
        break
    try:
        num = int(x)
        numbers.append(num)
        count += 1
    except:
        print("Nisi unio broj ni Done!")

print(f"Average: {Average(numbers)}")
print(f"Min: {min(numbers)}")
print(f"Max: {max(numbers)}")

numbers.sort()
print(numbers)