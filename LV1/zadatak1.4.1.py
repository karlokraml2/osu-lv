"""Napišite program koji od korisnika zahtijeva unos radnih sati te koliko je placen ´
po radnom satu. Koristite ugradenu Python metodu ¯ input(). Nakon toga izracunajte koliko ˇ
je korisnik zaradio i ispišite na ekran. Na kraju prepravite rješenje na nacin da ukupni iznos ˇ
izracunavate u zasebnoj funkciji naziva ˇ total_euro."""

def total_euro(working_hours, salary_per_hour):
    return working_hours*salary_per_hour

working_hours = int(input("Unesi broj radnih sati: "))
salary_per_hour = float(input("Unesi plaću po satu: "))

print(f"Radni sati: {working_hours}")
print(f"eura/h: {salary_per_hour}")
print(f"Ukupno: {total_euro(working_hours, salary_per_hour)} eura")
