""" Zadatak 3.4.1 Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
 Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljedeca pitanja:
 a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili
 duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip
 category.
 b) Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
 ime proizvodaca, model vozila i kolika je gradska potrošnja.
 c) Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija
 plinova za ova vozila?
 d) Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02
 plinova automobila proizvodaca Audi koji imaju 4 cilindara?
 e) Koliko je vozila s 4,6,8... cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na
 broj cilindara?
 f) Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila
 koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
 g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?
 h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?
 i) Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.
 """

import pandas as pd

#a)
data = pd.read_csv('data_C02_emission.csv')
print(f"DataFrame sadrži {len(data)} mjerenja.") #broj redova, može i data.shape[0]
print(f"Tipovi veličina: \n{data.dtypes}")
print(data.isnull().sum()) # nema izostalih vrijednosti
data.drop_duplicates() #nema dupliciranih vrijednosti
columns_to_convert = ['Make','Model','Vehicle Class','Transmission','Fuel Type']
data[columns_to_convert] = data[columns_to_convert].astype('category')
print(data.dtypes)

#b)
data_sorted = data.sort_values(by=['Fuel Consumption City (L/100km)'])
print(f"Tri auta s najvećom gradskom potrošnjom: \n{data_sorted[['Make','Model','Fuel Consumption City (L/100km)']].tail(3)}")
print(f"Tri auta s najmanjom gradskom potrošnjom: \n{data_sorted[['Make','Model','Fuel Consumption City (L/100km)']].head(3)}")

#c)
filtered_vehicles = data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] < 3.5)]
print(f"{filtered_vehicles.shape[0]} vozila ima veličinu motora između 2.5 i 3.5 L.")
print(f"Prosječna CO2 emisija plinova za ova vozila je {filtered_vehicles['CO2 Emissions (g/km)'].mean().__round__(2)}")

#d)
audi = data[data['Make'] == 'Audi']
print(f"{audi.shape[0]} mjerenja se odnosi na vozila proizvođača Audi.")
audi_4_cylinders = audi[audi['Cylinders'] == 4]
print(f"Prosječna emisija CO2 plinova automobila proizvođača Audi koji imaju 4 cilindra je {audi_4_cylinders['CO2 Emissions (g/km)'].mean().__round__(2)}")

#e)
data_groupedby_cylinders = data.groupby('Cylinders')
data_groupedby_even_cylinders = data[data['Cylinders'] % 2 == 0].groupby('Cylinders')
print(f"Broj vozila prema broju cilindara: \n{data_groupedby_cylinders.size()}")
# ako se misli samo na vozila s parnim brojem cilindara onda: 
print(f"Broj vozila prema broju cilindara (samo parni): \n{data_groupedby_even_cylinders.size()}")
print(f"Prosječna emisija CO2 plinova prema broju cilindara: \n{data_groupedby_cylinders['CO2 Emissions (g/km)'].mean().__round__(2)}")

#f)
diesel_vehicles = data[data['Fuel Type'] == 'D']
regular_gasoline_vehicles = data[data['Fuel Type'] == 'X']
print(f"Prosječna gradska potrošnja za dizel vozila je {diesel_vehicles['Fuel Consumption City (L/100km)'].mean().__round__(2)}, a za regularni benzin {regular_gasoline_vehicles['Fuel Consumption City (L/100km)'].mean().__round__(2)}")
print(f"Medijalne vrijednosti: Dizel - {diesel_vehicles['Fuel Consumption City (L/100km)'].median().__round__(2)}, Benzin - {regular_gasoline_vehicles['Fuel Consumption City (L/100km)'].median().__round__(2)}")

#g)
print(f"Dizel vozilo s 4 cilindra s najvećom gradskom potrošnjom: \n{diesel_vehicles[diesel_vehicles['Cylinders'] == 4].sort_values(by='Fuel Consumption City (L/100km)').tail(1)}")

#h)
manual_transmission_vehicles = data[data['Transmission'].apply(lambda x: x.startswith('M'))]
print(f"{manual_transmission_vehicles.shape[0]} vozila ima ručni tip mjenjača.")

#i)
print(data.corr(numeric_only=True))

