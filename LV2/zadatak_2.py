"""Zadatak 2.4.2 Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
 ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja data pricemu je u
 prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci
 stupac polja je masa u kg.
 a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?
 b) Prikažite odnos visine i mase osobe pomocu naredbe matplotlib.pyplot.scatter.
 c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
 d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom
 podatkovnom skupu.
 e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
 muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
 ind = (data[:,0] == 1)"""
import numpy as np
import matplotlib.pyplot as plt

file = open('data.csv')

""" lines = file.readlines()

data = []

for line in lines[1:]:
    line = line.rstrip()
    words = line.split(',')
    data.append(words)

array = np.array(data).astype(float)  """

array = np.array(np.loadtxt(file, delimiter=',', skiprows=1))

# a)
print(f"Mjerenje je izvršeno na {array.shape[0]} osoba.")
# b)
plt.scatter(x=array[:,1], y=array[:,2], marker='o', c='blue')
plt.xlabel("visina")
plt.ylabel("masa")
plt.show()
# c)
plt.scatter(x=array[49::50,1], y=array[49::50,2], marker='o', c='blue')
plt.xlabel("visina")
plt.ylabel("masa")
plt.show()
# d)
print(array[:,1].astype(float).min())
print(array[:,1].astype(float).max())
print(array[:,1].astype(float).mean())
# e)
ind = array[:,0].astype(float) == 1.0
print("Samo muškarci: ")
print(f"Min: {array[ind,1].astype(float).min()}")
print(f"Max: {array[ind,1].astype(float).max()}")
print(f"Mean: {array[ind,1].astype(float).mean()}")

ind = array[:,0].astype(float) == 0.0
print("Samo žene: ")
print(f"Min: {array[ind,1].astype(float).min()}")
print(f"Max: {array[ind,1].astype(float).max()}")
print(f"Mean: {array[ind,1].astype(float).mean()}")
